/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    definition,
    pgettext,
    tripetto,
} from "tripetto";
import { MultipleChoice } from "..";
import { Choice } from "../choice";

/** Assets */
import ICON from "../../../assets/condition.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    alias: "multiple-choice",
    context: MultipleChoice,
    icon: ICON,
    get label() {
        return pgettext("block:multiple-choice", "Choice");
    },
})
export class MultipleChoiceCondition extends ConditionBlock {
    @affects("#condition")
    @definition("choices")
    choice: Choice | undefined;

    get name() {
        return (this.choice ? this.choice.name : "") || this.type.label;
    }

    get label() {
        return this.node && this.node.block instanceof MultipleChoice
            ? this.node.block.alias || this.node.label || ""
            : "";
    }
}
