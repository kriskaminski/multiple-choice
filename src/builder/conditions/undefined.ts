/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionBlock, pgettext, tripetto } from "tripetto";
import { MultipleChoice } from "..";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:undefined`,
    version: PACKAGE_VERSION,
    context: MultipleChoice,
    icon: ICON,
    get label() {
        return pgettext("block:multiple-choice", "No choice made");
    },
})
export class MultipleChoiceUndefinedCondition extends ConditionBlock {}
