/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    NodeBlock,
    REGEX_IS_URL,
    Slots,
    affects,
    conditions,
    definition,
    editor,
    isBoolean,
    isString,
    npgettext,
    pgettext,
    slotInsertAction,
    slots,
    tripetto,
} from "tripetto";
import { Choice } from "./choice";
import { MultipleChoiceCondition } from "./conditions/choice";
import { MultipleChoiceUndefinedCondition } from "./conditions/undefined";

/** Assets */
import ICON from "../../assets/icon.svg";
import { StringSlot } from "@tripetto/slots/lib/types/string/string";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    alias: "multiple-choice",
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:multiple-choice", "Multiple choice");
    },
})
export class MultipleChoice extends NodeBlock {
    @definition
    caption?: string;

    @definition
    imageURL?: string;

    @definition
    imageWidth?: string;

    @definition
    imageAboveText?: boolean;

    @definition
    @affects("#name")
    choices = Collection.of(Choice, this as MultipleChoice);

    @definition
    @affects("#slots")
    @affects("#collection", "choices")
    multiple?: boolean;

    @definition
    alignment?: boolean;

    @definition
    @affects("#required")
    @affects("#slots")
    @affects("#collection", "choices")
    required?: boolean;

    @definition
    @affects("#slots")
    @affects("#collection", "choices")
    @affects("#label")
    alias?: string;

    @definition
    @affects("#slots")
    @affects("#collection", "choices")
    exportable?: boolean;

    get label() {
        return npgettext(
            "block:multiple-choice",
            "%2 (%1 option)",
            "%2 (%1 options)",
            this.choices.count,
            this.type.label
        );
    }

    @slots
    defineSlot(): void {
        if (this.multiple) {
            this.slots.delete("choice", "static");
        } else {
            this.slots.static({
                type: Slots.String,
                reference: "choice",
                label: pgettext("block:multiple-choice", "Choice"),
                alias: this.alias,
                required: this.required,
                exportable: this.exportable,
            });
        }
    }

    @editor
    defineEditor(): void {
        this.editor.name(true, true);
        this.editor.option({
            name: pgettext("block:multiple-choice", "Caption"),
            form: {
                title: pgettext("block:multiple-choice", "Caption"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "caption", undefined)
                    )
                        .placeholder(
                            pgettext(
                                "block:multiple-choice",
                                "Type caption text here..."
                            )
                        )
                        .action("@", slotInsertAction(this)),
                ],
            },
            activated: isString(this.caption),
        });
        this.editor.description();
        this.editor.option({
            name: pgettext("block:multiple-choice", "Image"),
            form: {
                title: pgettext("block:multiple-choice", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(
                            pgettext(
                                "block:multiple-choice",
                                "Image source URL"
                            )
                        )
                        .inputMode("url")
                        .placeholder("https://")
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(
                            pgettext(
                                "block:multiple-choice",
                                "Image width (optional)"
                            )
                        )
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:multiple-choice",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });
        this.editor.explanation();
        this.editor.collection({
            collection: this.choices,
            title: pgettext("block:multiple-choice", "Choices"),
            placeholder: pgettext("block:multiple-choice", "Unnamed choice"),
            editable: true,
            sorting: "manual",
        });

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:multiple-choice", "Multiple select"),
            form: {
                title: pgettext("block:multiple-choice", "Multiple select"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:multiple-choice",
                            "Allow the selection of multiple answers"
                        ),
                        Forms.Checkbox.bind(this, "multiple", undefined, true)
                    ),
                ],
            },
            activated: isBoolean(this.multiple),
        });
        this.editor.option({
            name: pgettext("block:multiple-choice", "Alignment"),
            form: {
                title: pgettext("block:multiple-choice", "Alignment"),
                controls: [
                    new Forms.Radiobutton(
                        [
                            {
                                label: pgettext(
                                    "block:multiple-choice",
                                    "Vertically (top-down)"
                                ),
                                value: false,
                            },
                            {
                                label: pgettext(
                                    "block:multiple-choice",
                                    "Horizontally (left-right)"
                                ),
                                value: true,
                            },
                        ],
                        Forms.Radiobutton.bind(this, "alignment", undefined)
                    ),
                ],
            },
            activated: isBoolean(this.alignment),
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();
        this.editor.alias(this);
        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        this.choices.each((choice: Choice) => {
            if (choice.name) {
                this.conditions.template({
                    condition: MultipleChoiceCondition,
                    label: choice.name,
                    props: {
                        choice: choice,
                        slot: this.slots.select(
                            this.multiple ? choice.id : "choice"
                        ),
                    },
                });
            }
        });

        if (this.choices.count > 0) {
            this.conditions.template({
                condition: MultipleChoiceUndefinedCondition,
            });
        }
    }
}
