/** Dependencies */
import {
    Collection,
    Components,
    Forms,
    REGEX_IS_URL,
    Slots,
    affects,
    created,
    definition,
    deleted,
    editor,
    isBoolean,
    isString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
} from "tripetto";
import { MultipleChoice } from "./";

export class Choice extends Collection.Item<MultipleChoice> {
    @definition
    @name
    name = "";

    @definition
    description?: string;

    @definition
    url?: string;

    @definition
    @affects("#refresh")
    moniker?: string;

    @definition
    @affects("#refresh")
    value?: string;

    @definition
    exclusive?: boolean;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): void {
        if (this.ref.multiple) {
            this.ref.slots.dynamic({
                type: Slots.Boolean,
                reference: this.id,
                label: pgettext("block:multiple-choice", "Choice"),
                sequence: this.index,
                name: this.moniker || this.name,
                alias: this.value,
                required: this.ref.required,
                exportable: this.ref.exportable,
                pipeable: {
                    group: "Choice",
                    label: pgettext("block:multiple-choice", "Choice"),
                    template: "name",
                    alias: this.ref.alias,
                },
            });
        } else {
            this.deleteSlot();
        }
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:multiple-choice", "Label"),
            form: {
                title: pgettext("block:multiple-choice", "Choice label"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .autoFocus()
                        .autoSelect(),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:multiple-choice", "Description"),
            form: {
                title: pgettext("block:multiple-choice", "Choice description"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "description", undefined)
                    ),
                ],
            },
            activated: isString(this.description),
        });

        this.editor.group(pgettext("block:multiple-choice", "Options"));

        this.editor.option({
            name: pgettext("block:multiple-choice", "URL"),
            form: {
                title: pgettext("block:multiple-choice", "URL"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "url", undefined)
                    )
                        .placeholder("https://")
                        .autoValidate((url: Forms.Text) =>
                            url.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(url.value)
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Static(
                        pgettext(
                            "block:multiple-choice",
                            // tslint:disable-next-line:max-line-length
                            "If a URL is set, clicking the choice will open it. The choice cannot be selected as answer."
                        )
                    ),
                ],
            },
            activated: isString(this.url),
            on: (urlFeature: Components.Feature<Forms.Form>) => {
                monikerFeature.disabled(urlFeature.isActivated);
                identifierFeature.disabled(urlFeature.isActivated);
                exclusivityFeature.disabled(
                    urlFeature.isActivated || !this.ref.multiple
                );
            },
        });

        const monikerFeature = this.editor.option({
            name: pgettext("block:multiple-choice", "Moniker"),
            form: {
                title: pgettext("block:multiple-choice", "Choice moniker"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "moniker", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:multiple-choice",
                            "If a moniker is set, this moniker will be used when referred to this choice."
                        )
                    ),
                ],
            },
            activated: isString(this.moniker),
            disabled: isString(this.url),
        });

        const exclusivityFeature = this.editor.option({
            name: pgettext("block:multiple-choice", "Exclusivity"),
            form: {
                title: pgettext("block:multiple-choice", "Choice exclusivity"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:multiple-choice",
                            "Unselect all other selected choices when selected"
                        ),
                        Forms.Checkbox.bind(this, "exclusive", undefined, true)
                    ),
                ],
            },
            activated:
                (this.ref.multiple &&
                    !isString(this.url) &&
                    isBoolean(this.exclusive)) ||
                false,
            disabled: !this.ref.multiple || isString(this.url),
        });

        const identifierFeature = this.editor.option({
            name: pgettext("block:multiple-choice", "Identifier"),
            form: {
                title: pgettext("block:multiple-choice", "Choice identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:multiple-choice",
                            "If a choice identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
            disabled: isString(this.url),
        });
    }
}
