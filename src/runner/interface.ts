export interface IChoice {
    readonly id: string;
    readonly name: string;
    readonly description?: string;
    readonly url?: string;
    readonly value?: string;
    readonly exclusive?: boolean;
}

export interface IMultipleChoice {
    readonly caption?: string;
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
    readonly choices: IChoice[];
    readonly multiple?: boolean;
    readonly alignment?: boolean;
    readonly required?: boolean;
}

export interface IChoiceCondition {
    readonly choice: string | undefined;
}
